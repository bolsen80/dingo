package org.dingo.lang.parser;

import org.dingo.lang.DingoToken;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AstPrinterTest {
    final Map<String, Expr> samples = Map.of(
            "(+ 1 2)",
            new Expr.Binary(new Expr.Literal(1),
                    new DingoToken(DingoToken.Type.PLUS, "+", null,1),
                    new Expr.Literal(2))
    );

    @Test
    public void testSamples() {
        for (Map.Entry<String, Expr> expr : samples.entrySet()) {
            String result = new AstPrinter().run(expr.getValue());
            assertEquals(expr.getKey(), result);
        }
    }

}