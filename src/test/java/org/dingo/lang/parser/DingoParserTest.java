package org.dingo.lang.parser;

import org.dingo.lang.DingoLexer;
import org.dingo.lang.DingoToken;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DingoParserTest {

    /**
     * We test an AstPrinter output, which outputs a semi-Lisp language,
     * loosely defined in the Lox book, but more detailed here since
     * Lispy syntax is a good way to compare ASTs visually.
     *
     * Example: assertAstMatches("1 + 1;", "(+ 1.0 1.0)"); => true
     * @param sample
     * @param ast
     */
    private static void assertAstMatches(String sample, String ast) {
        var lexer = new DingoLexer(sample);
        List<DingoToken> lexerResult = lexer.scanForTokens();
        var parser = new DingoParser(lexerResult);
        var astPrinter = new AstPrinter();
        String parseResult = astPrinter.run(parser.parse());
        assertEquals(ast, parseResult);
    }

    @Test
    public void testSimpleExpressions() {
        assertAstMatches("1 + 1;", "(+ 1.0 1.0)");
        assertAstMatches("1 - 1;", "(- 1.0 1.0)");
        assertAstMatches("1 * 1;", "(* 1.0 1.0)");
        assertAstMatches("1 / 1;", "(/ 1.0 1.0)");
        assertAstMatches("1 == 1;", "(== 1.0 1.0)");
        assertAstMatches("1 != 1;", "(!= 1.0 1.0)");
        assertAstMatches("1 > 1;", "(> 1.0 1.0)");
        assertAstMatches("1 >= 1;", "(>= 1.0 1.0)");
        assertAstMatches("1 < 1;", "(< 1.0 1.0)");
        assertAstMatches("1 <= 1;", "(<= 1.0 1.0)");
    }

    @Test
    public void testWithGroups() {
        // In our "Lisp", functions must evaluate first before operators.
        assertAstMatches("1 + 1 - (2 + 2);", "(- (+ 1.0 1.0) (group (+ 2.0 2.0)))");
        // Do 1 - 2 in group, then 1 + (1 - 2) then 1 + (1 - 2) + 2
        assertAstMatches("1 + (1 - 2) + 2;", "(+ (+ 1.0 (group (- 1.0 2.0))) 2.0)");
    }
}