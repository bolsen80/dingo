package org.dingo.lang;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.dingo.lang.DingoToken.Type.BANG;
import static org.dingo.lang.DingoToken.Type.BANG_EQUAL;
import static org.dingo.lang.DingoToken.Type.COMMA;
import static org.dingo.lang.DingoToken.Type.DOLLAR_SIGN;
import static org.dingo.lang.DingoToken.Type.EOF;
import static org.dingo.lang.DingoToken.Type.EQUAL;
import static org.dingo.lang.DingoToken.Type.EQUAL_EQUAL;
import static org.dingo.lang.DingoToken.Type.GREATER;
import static org.dingo.lang.DingoToken.Type.GREATER_EQUAL;
import static org.dingo.lang.DingoToken.Type.HASH;
import static org.dingo.lang.DingoToken.Type.IDENTIFIER;
import static org.dingo.lang.DingoToken.Type.LEFT_CURLY;
import static org.dingo.lang.DingoToken.Type.LEFT_PAREN;
import static org.dingo.lang.DingoToken.Type.LESS;
import static org.dingo.lang.DingoToken.Type.LESS_EQUAL;
import static org.dingo.lang.DingoToken.Type.MEMBER;
import static org.dingo.lang.DingoToken.Type.MINUS;
import static org.dingo.lang.DingoToken.Type.MINUS_EQUALS;
import static org.dingo.lang.DingoToken.Type.NUMBER;
import static org.dingo.lang.DingoToken.Type.PLUS_EQUALS;
import static org.dingo.lang.DingoToken.Type.RIGHT_CURLY;
import static org.dingo.lang.DingoToken.Type.RIGHT_PAREN;
import static org.dingo.lang.DingoToken.Type.SEMICOLON;
import static org.dingo.lang.DingoToken.Type.SLASH;
import static org.dingo.lang.DingoToken.Type.SLASH_EQUALS;
import static org.dingo.lang.DingoToken.Type.STAR_EQUALS;
import static org.dingo.lang.DingoToken.Type.STRING;
import static org.junit.jupiter.api.Assertions.*;

class DingoLexerTest {
    static Map<String, List<DingoToken.Type>> samples;

    DingoLexerTest() {
        samples = Map.of("""
var("set_name" = false);
if(action_param("name") != null);
   $set_name = true;
/if;""", List.of(IDENTIFIER, LEFT_PAREN, STRING, EQUAL, IDENTIFIER, RIGHT_PAREN, SEMICOLON,
                IDENTIFIER, LEFT_PAREN, IDENTIFIER, LEFT_PAREN, STRING, RIGHT_PAREN, BANG_EQUAL, IDENTIFIER, RIGHT_PAREN, SEMICOLON,
                DOLLAR_SIGN, IDENTIFIER, EQUAL, IDENTIFIER, SEMICOLON,
                SLASH, IDENTIFIER, SEMICOLON,
                EOF),

"""
while(#i > 0);
   print(true);
/while;""", List.of(IDENTIFIER, LEFT_PAREN, HASH, IDENTIFIER, GREATER, NUMBER, RIGHT_PAREN, SEMICOLON,
                        IDENTIFIER, LEFT_PAREN, IDENTIFIER, RIGHT_PAREN, SEMICOLON,
                        SLASH, IDENTIFIER, SEMICOLON,
                        EOF),
"""
while(#i >= 0);
   print(true);
/while;""", List.of(IDENTIFIER, LEFT_PAREN, HASH, IDENTIFIER, GREATER_EQUAL, NUMBER, RIGHT_PAREN, SEMICOLON,
                        IDENTIFIER, LEFT_PAREN, IDENTIFIER, RIGHT_PAREN, SEMICOLON,
                        SLASH, IDENTIFIER, SEMICOLON,
                        EOF),
"""
// comment
""", List.of(EOF),
"""
// comment
run_code('test');
""", List.of(IDENTIFIER, LEFT_PAREN, STRING, RIGHT_PAREN, SEMICOLON, EOF),

// If we see a minus and an identifier, it could mean that it is a parameter in a function OR making a variable negative.
// The parser must decide what this actually means based on the context.
"""
params(-test=1, -foobar=2);
""", List.of(IDENTIFIER, LEFT_PAREN, MINUS, IDENTIFIER, EQUAL, NUMBER, COMMA, MINUS, IDENTIFIER, EQUAL, NUMBER, RIGHT_PAREN, SEMICOLON, EOF),

"""
assert(1 > 0);
assert(1 >= 0);
assert(1 < 0);
assert(1 <= 0);
assert(1 == 0);
assert(1 != 0);
""", List.of(IDENTIFIER, LEFT_PAREN, NUMBER, GREATER, NUMBER, RIGHT_PAREN, SEMICOLON,
                        IDENTIFIER, LEFT_PAREN, NUMBER, GREATER_EQUAL, NUMBER, RIGHT_PAREN, SEMICOLON,
                        IDENTIFIER, LEFT_PAREN, NUMBER, LESS, NUMBER, RIGHT_PAREN, SEMICOLON,
                        IDENTIFIER, LEFT_PAREN, NUMBER, LESS_EQUAL, NUMBER, RIGHT_PAREN, SEMICOLON,
                        IDENTIFIER, LEFT_PAREN, NUMBER, EQUAL_EQUAL, NUMBER, RIGHT_PAREN, SEMICOLON,
                        IDENTIFIER, LEFT_PAREN, NUMBER, BANG_EQUAL, NUMBER, RIGHT_PAREN, SEMICOLON,
                        EOF),
"""
var('my_expr' = { $my_var += 6 });        
""", List.of(IDENTIFIER, LEFT_PAREN, STRING, EQUAL, LEFT_CURLY, DOLLAR_SIGN, IDENTIFIER, PLUS_EQUALS, NUMBER, RIGHT_CURLY, RIGHT_PAREN, SEMICOLON, EOF),

"""
!a;
-a;
a += 1;
a -= 1;
a *= 1;
a /= 1;        
""", List.of(BANG, IDENTIFIER, SEMICOLON,
                        MINUS, IDENTIFIER, SEMICOLON,
                        IDENTIFIER, PLUS_EQUALS, NUMBER, SEMICOLON,
                        IDENTIFIER, MINUS_EQUALS, NUMBER, SEMICOLON,
                        IDENTIFIER, STAR_EQUALS, NUMBER, SEMICOLON,
                        IDENTIFIER, SLASH_EQUALS, NUMBER, SEMICOLON,
                        EOF),
"""
$a->eval();""", List.of(DOLLAR_SIGN, IDENTIFIER, MEMBER, IDENTIFIER, LEFT_PAREN, RIGHT_PAREN, SEMICOLON, EOF)
        );
    }/**/

    @Test
    void testCodeSamples() {
        for (String sample: samples.keySet()) {
            var lexer = new DingoLexer(sample);
            List<DingoToken.Type> expectedResult = samples.get(sample);
            List<DingoToken.Type> result = lexer.scanForTokens().stream().map(dt -> dt.type).toList();
            assertEquals(expectedResult, result);
        }
    }
}