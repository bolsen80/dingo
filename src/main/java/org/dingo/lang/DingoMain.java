package org.dingo.lang;

import org.dingo.lang.parser.AstPrinter;
import org.dingo.lang.parser.DingoParser;
import org.dingo.lang.parser.Expr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class DingoMain {
    static boolean hadError = false;

    public static void main(String[] args) throws IOException {
        if (args.length == 1) {
            runDingoSourceFile(args[0]);
            return;
        }

        if (args.length == 0) {
            runDingoRepl();
        }

        if (args.length > 1) {
            System.out.println("Usage: dingo [file]");
            System.exit(64);
        }
    }

    public static void runDingoSourceFile(String fileName) throws IOException {
        byte[] bytes = Files.readAllBytes(Paths.get(fileName));
        runSource(new String(bytes, Charset.defaultCharset()));
        if (hadError) System.exit(65);
    }

    public static void runDingoRepl() throws IOException {
        var input = new InputStreamReader(System.in);
        var reader = new BufferedReader(input);
        while (true) {
            System.out.print("> ");
            var line = reader.readLine();
            if (line == null) break;
            runSource(line);
            hadError = false;
        }
    }

    public static void runSource(String source) {
        var lexer = new DingoLexer(source);
        List<DingoToken> tokens = lexer.scanForTokens();
        DingoParser parser = new DingoParser(tokens);
        Expr expr = parser.parse();

        if (hadError) return;

        System.out.println(new AstPrinter().run(expr));
        for (DingoToken token: tokens) {
            System.out.println(token);
        }
    }

}