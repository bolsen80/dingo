package org.dingo.lang.parser;

public class AstPrinter implements Expr.Visitor<String> {
    @Override
    public String visitBinary(Expr.Binary binary) {
        return print(binary.operator.lexeme, binary.left, binary.right);
    }

    @Override
    public String visitGrouping(Expr.Grouping grouping) {
        return print("group", grouping.expr);
    }

    @Override
    public String visitLiteral(Expr.Literal literal) {
        return literal.value.toString();
    }

    @Override
    public String visitUnary(Expr.Unary unary) {
        return print(unary.operator.lexeme, unary.right);
    }

    @Override
    public String visitIdentifier(Expr.Identifier rIdentifier) {
        return "(id " + rIdentifier.identifier.lexeme + ")";
    }

    public String run(Expr expr) {
        return expr.accept(this);
    }
    public String print(String name, Expr... exprs) {
        var builder = new StringBuilder();
        builder.append("(").append(name);
        for (Expr expr : exprs) {
            builder.append(" ");
            builder.append(expr.accept(this));
        }
        builder.append(")");
        return builder.toString();
    }
}
