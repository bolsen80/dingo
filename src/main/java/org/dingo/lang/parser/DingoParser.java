package org.dingo.lang.parser;

import org.dingo.lang.DingoToken;
import org.dingo.lang.Logging;

import java.util.List;

import static org.dingo.lang.DingoToken.Type.BANG_EQUAL;
import static org.dingo.lang.DingoToken.Type.EOF;
import static org.dingo.lang.DingoToken.Type.EQUAL_EQUAL;
import static org.dingo.lang.DingoToken.Type.GREATER;
import static org.dingo.lang.DingoToken.Type.GREATER_EQUAL;
import static org.dingo.lang.DingoToken.Type.IDENTIFIER;
import static org.dingo.lang.DingoToken.Type.LEFT_PAREN;
import static org.dingo.lang.DingoToken.Type.LESS;
import static org.dingo.lang.DingoToken.Type.LESS_EQUAL;
import static org.dingo.lang.DingoToken.Type.MINUS;
import static org.dingo.lang.DingoToken.Type.NUMBER;
import static org.dingo.lang.DingoToken.Type.PLUS;
import static org.dingo.lang.DingoToken.Type.RIGHT_PAREN;
import static org.dingo.lang.DingoToken.Type.SLASH;
import static org.dingo.lang.DingoToken.Type.STAR;
import static org.dingo.lang.DingoToken.Type.STRING;
import static org.dingo.lang.DingoToken.Type.SEMICOLON;

/**
 * expression     → equality ;
 * equality       → comparison ( ( "!=" | "==" ) comparison )* ;
 * comparison     → term ( ( ">" | ">=" | "<" | "<=" ) term )* ;
 * term           → factor ( ( "-" | "+" ) factor )* ;
 * factor         → unary ( ( "/" | "*" ) unary )* ;
 * unary          → ( "!" | "-" ) unary
 *                  | primary ;
 * primary        → NUMBER | STRING | "true" | "false" | "nil"
 *                  | "(" expression ")" ;
 */

/* TODO: Missing syntax elements:
 * $var, #var,
 */

public class DingoParser {
    private static class ParseError extends RuntimeException {}
    private final List<DingoToken> tokens;
    private int current = 0;

    public DingoParser(List<DingoToken> tokens) {
        this.tokens = tokens;
    }

    public Expr parse() {
        try {
            return expression();
        } catch (ParseError error) {
            return null;
        }
    }

    private Expr expression() {
        return equality();
    }

    private Expr equality() {
        Expr expr = comparison();

        while(matchToken(BANG_EQUAL, EQUAL_EQUAL)) {
            DingoToken operator = previous();
            Expr right = comparison();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr comparison() {
        Expr expr = term();

        while (matchToken(GREATER, GREATER_EQUAL, LESS, LESS_EQUAL)) {
            DingoToken operator = previous();
            Expr right = term();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr term() {
        Expr expr = factor();

        while (matchToken(MINUS, PLUS)) {
            DingoToken operator = previous();
            Expr right = factor();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr factor() {
        Expr expr = unary();

        while (matchToken(SLASH, STAR)) {
            DingoToken operator = previous();
            Expr right = unary();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr unary() {
        if (matchToken(SLASH, STAR)) {
            DingoToken operator = previous();
            Expr right = unary();
            return new Expr.Unary(operator, right);
        }

        return primary();
    }

    private Expr primary() {
        if (matchToken(IDENTIFIER)) {
            DingoToken identifier = previous();
            switch(identifier.lexeme) {
                case "true":
                    return new Expr.Literal(true);
                case "false":
                    return new Expr.Literal(false);
                case "null":
                    return new Expr.Literal(null);
                default:
                    return new Expr.Identifier(identifier);
            }
        }

        if (matchToken(NUMBER, STRING)) {
            return new Expr.Literal(previous().literal);
        }

        if (matchToken(LEFT_PAREN)) {
            Expr expr = expression();
            consume(RIGHT_PAREN, "Expect ')' after expression.");
            return new Expr.Grouping(expr);
        }

        throw error(peek(), "Expecting an expression");
    }

    // Helpers
    private boolean matchToken(DingoToken.Type... tokens) {
        for (DingoToken.Type token: tokens) {
            if (peek().type == EOF) { return false; }
            if (peek().type == token) {
                advanceToken();
                return true;
            }
        }

        return false;
    }

    private DingoToken advanceToken() {
        if (peek().type != EOF) current++;
        return previous();
    }

    private DingoToken previous() {
        return tokens.get(current - 1);
    }

    private DingoToken peek() {
        return tokens.get(current);
    }

    private DingoToken consume(DingoToken.Type token, String message) {
        if (peek().type == token) {
            return advanceToken();
        }
        throw error(peek(), message);
    }

    private ParseError error(DingoToken token, String message) {
        Logging.log(Logging.LogType.ERROR, 0, message);
        return new ParseError();
  }

  private void synchronize() {
        advanceToken();
        while(peek().type != EOF) {
            if (previous().type == SEMICOLON) {
                return;
            }
            advanceToken();
        }
  }
}
