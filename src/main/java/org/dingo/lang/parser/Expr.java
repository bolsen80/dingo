package org.dingo.lang.parser;

import org.dingo.lang.DingoToken;

/**
 * literal: string | number | "true" | "false" | "null" ;
 * string: '"' . '"' | '\'' . '\''
 * number: [0-9]+ ('.' [0-9]+) ;
 *
 * expr: var | local_var | literal | unary | binary | grouping ;
 * unary: ("-" | "!") expr ;
 * binary: expr operator expr ;
 * grouping: "(" expr ")" ;
 * var: '$' identifier ('=' stmt) ;
 * local_var: '#' identiifer ('=' stmt) ;
 */

public abstract class Expr {

    interface Visitor<R> {
        R visitBinary(Binary binary);
        R visitGrouping(Grouping grouping);
        R visitLiteral(Literal literal);
        R visitUnary(Unary unary);
        R visitIdentifier(Identifier identifier);

    }

    abstract <R> R accept(Visitor<R> visitor);
    static class Binary<R> extends Expr {
            final Expr left;
            final DingoToken operator;
            final Expr right;

        Binary(Expr left, DingoToken operator, Expr right) {
            this.left = left;
            this.operator = operator;
            this.right = right;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitBinary(this);
        }
    }

    static class Grouping<R> extends Expr {
        final Expr expr;

        Grouping(Expr expr) {
            this.expr = expr;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitGrouping(this);
        }

    }

    public static class Literal<R> extends Expr {
            final Object value;

        public Literal(Object value) {
            this.value = value;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitLiteral(this);
        }
    }

    public static class Unary<R> extends Expr{
        final DingoToken operator;
        final Expr right;

        public Unary(DingoToken operator, Expr right) {
            this.operator = operator;
            this.right = right;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitUnary(this);
        }
    }

    public static class Identifier<R> extends Expr{
        final DingoToken identifier;

        public Identifier(DingoToken identifier) {
            this.identifier = identifier;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitIdentifier(this);
        }
    }
}