package org.dingo.lang;

import java.util.ArrayList;
import java.util.List;

public class DingoLexer {
    private final String source;
    private final List<DingoToken> tokens = new ArrayList<>();
    int current = 0;
    int start = 0;
    int line = 1;
    public DingoLexer(String source) {
        this.source = source;
    }

    public List<DingoToken> scanForTokens() {
        while (current < source.length()) { // loop until the end
            start = current;
            findNextToken();
        }

        tokens.add(new DingoToken(DingoToken.Type.EOF, "", null, line));
        return tokens;
    }

    private void findNextToken() {
        char scanChar = advanceChar();
        switch (scanChar) {
            case '\r':
            case '\t':
            case ' ':
                break;
            case '\n':
                // TODO: Might want to check with System.getProperty("line.separator") instead.
                line++;
                break;

            case '(':
                addToken(DingoToken.Type.LEFT_PAREN, null);
                break;
            case ')':
                addToken(DingoToken.Type.RIGHT_PAREN, null);
                break;
            case '{':
                addToken(DingoToken.Type.LEFT_CURLY, null);
                break;
            case '}':
                addToken(DingoToken.Type.RIGHT_CURLY, null);
                break;
            case ',':
                addToken(DingoToken.Type.COMMA, null);
                break;
            case '$':
                addToken(DingoToken.Type.DOLLAR_SIGN, null);
                break;
            case '#':
                addToken(DingoToken.Type.HASH, null);
                break;
            case ';':
                addToken(DingoToken.Type.SEMICOLON, null);
                break;
            case '-':
                if (matchAhead('=')) {
                    // -=
                    addToken(DingoToken.Type.MINUS_EQUALS, null);
                } else if (matchAhead('>')) {
                    // ->
                    addToken(DingoToken.Type.MEMBER, null);
                } else {
                    addToken(DingoToken.Type.MINUS, null);
                }
                break;
            case '+':
                addToken(matchAhead('=') ? DingoToken.Type.PLUS_EQUALS : DingoToken.Type.PLUS, null); break;
            case '*':
                addToken(matchAhead('=') ? DingoToken.Type.STAR_EQUALS : DingoToken.Type.STAR, null); break;
            case '!':
                addToken(matchAhead('=') ? DingoToken.Type.BANG_EQUAL : DingoToken.Type.BANG, null);
                break;
            case '=':
                addToken(matchAhead('=') ? DingoToken.Type.EQUAL_EQUAL : DingoToken.Type.EQUAL, null);
                break;
            case '<':
                addToken(matchAhead('=') ? DingoToken.Type.LESS_EQUAL : DingoToken.Type.LESS, null);
                break;
            case '>':
                addToken(matchAhead('=') ? DingoToken.Type.GREATER_EQUAL : DingoToken.Type.GREATER, null);
                break;

            case '/':
                if (matchAhead('/')) {
                    while (peekAhead() != '\n' && current <= source.length()) {
                        advanceChar();
                    }
                } else if (matchAhead('=')) {
                    addToken(DingoToken.Type.SLASH_EQUALS, null); break;
                } else {
                    addToken(DingoToken.Type.SLASH, null);
                }
                break;

            case '"': matchString('"'); break;
            case '\'': matchString('\''); break;

            default:
                if (isDigit(scanChar)) {
                    matchNumber();
                } else if (isAlpha(scanChar)) {
                    matchIdentifier();
                } else {
                    Logging.log(Logging.LogType.ERROR, line, "Unrecognized token: " + scanChar);
                }
                break;
        }
    }

    private char peekAhead() {
        if (current >= source.length()) return '\0';
        return source.charAt(current);
    }

    private char advanceChar() {
        return source.charAt(current++);
    }

    private boolean matchAhead(char lexeme) {
        if (current >= source.length()) return false;
        if (source.charAt(current) == lexeme) {
            current++;
            return true;
        }
        return false;
    }

    private void addToken(DingoToken.Type type, Object literal) {
        String text = source.substring(start, current);
        tokens.add(new DingoToken(type, text, literal, line));
    }

    private void matchString(char type) {
        while (peekAhead() != type && current <= source.length()) {
            if (peekAhead() == '\n') line++;
            advanceChar();
        }

        if (current >= source.length()) {
            Logging.log(Logging.LogType.ERROR, line, "Unterminated string.");
            return;
        }

        advanceChar(); // past final '"' or '\''

        var value = source.substring(start+1, current-1);
        addToken(DingoToken.Type.STRING, value);
    }

    private boolean isDigit(char lexeme) {
        return lexeme >= '0' && lexeme <= '9';
    }

    private boolean isAlpha(char lexeme) {
        return (lexeme >= 'A' && lexeme <= 'Z') || (lexeme >= 'a' && lexeme <= 'z') || lexeme == '_';
    }

    private boolean isAlphaNumeric(char lexeme) {
        return isAlpha(lexeme) || isDigit(lexeme);
    }

    private void matchNumber() {
        while (isDigit(peekAhead())) {
            advanceChar();
        }

        if (peekAhead() == '.' && isDigit(peekNext())) {
            advanceChar();

            while (isDigit(peekAhead())) {
                advanceChar();
            }
        }

        addToken(DingoToken.Type.NUMBER, Double.parseDouble(source.substring(start, current)));
    }

    private void matchIdentifier() {
        while (isAlphaNumeric(peekAhead()) || peekAhead() == '_') {
            advanceChar();
        }
        addToken(DingoToken.Type.IDENTIFIER, source.substring(start, current));
    }
    private char peekNext() {
        if (current + 1 >= source.length()) {
            return '\0';
        }
        return source.charAt(current + 1);
    }

}
