package org.dingo.lang;

import java.util.List;

public class DingoToken {

    public enum Type {
        LEFT_PAREN, RIGHT_PAREN, EQUAL, LEFT_BRACE, RIGHT_BRACE,
        LEFT_CURLY, RIGHT_CURLY, MINUS, PLUS, SEMICOLON, SLASH, STAR,
        DOLLAR_SIGN, COMMA,

        BANG, ARROW, GREATER, GREATER_EQUAL, LESS, LESS_EQUAL,

        IDENTIFIER, STRING, NUMBER,

        HASH, BANG_EQUAL, EQUAL_EQUAL, EOF,

        // no keywords!
        PLUS_EQUALS, MINUS_EQUALS, STAR_EQUALS, SLASH_EQUALS, MEMBER;
    }

    // A token has type, the actual data (lexeme), the data casted, and where it is in the source file.
    public Type type;
    public String lexeme;
    public Object literal;
    public int line;

    public DingoToken(Type type, String lexeme, Object literal, int line) {
        this.type = type;
        this.lexeme = lexeme;
        this.literal = literal;
        this.line = line;
    }

    /**
     * Helper to create an empty token for tests and internal comparisons.
     * @param tokenType
     * @return
     */
    public static DingoToken of(Type tokenType) {
        return new DingoToken(tokenType, "", null, 0);
    }

    public static DingoToken of(Type tokenType, String lexeme) {
        return new DingoToken(tokenType, lexeme, null, 0);
    }

    public static DingoToken of(Type tokenType, String lexeme, Object literal) {
        return new DingoToken(tokenType, lexeme, literal, 0);
    }

    public static DingoToken of(Type tokenType, String lexeme, Object literal, int line) {
        return new DingoToken(tokenType, lexeme, literal, line);
    }

    public static boolean typeEquals(DingoToken a, DingoToken b) {
        return a.type == b.type;
    }

    public String toString() {
        return type + " | " + lexeme + " | " + literal;
    }
}
