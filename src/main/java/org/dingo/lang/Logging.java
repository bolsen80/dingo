package org.dingo.lang;

public class Logging {
    public enum LogType {
        ERROR,
        WARN,
        DEBUG
   }
    public static void log(LogType logType, int line, String message) {
        System.err.println("[" + logType.toString() + "] | Line " + line + " | " + message);
    }
}
