# Dingo

Dingo is a work-in-progress. The project is both a walk-through of craftinginterpreters.com 
but also to make a language and runtime for a web-based language, reminiscent of the Lasso
language.

The plan is to make a simple languages for web applications that runs in a JEE container and
configured through an admin site. Semantically, it will be a dynamically-typed interpreter
(the question of byte-code is for later) that piggybacks on Java types (strings, numbers, bytes, etc.)

In terms of syntax, it is slightly reminiscent of Tcl: all functions are treated as commands, etc.

## BNF

This is not done. Just here for reference.

```
program: stmts ;
stmts: stmt (stmt+) ;
stmt: cmd_call | block_call | var | local_var | expr ;
cmd_call: identifier ;
identifier: [a-ZA-Z0-9_]+ ;
literal: string | number | "true" | "false" | "null" ;
string: '"' . '"' | '\'' . '\''
number: [0-9]+ ('.' [0-9]+) ;
operator: "==" | "!=" | "<" | "<=" | ">" | ">="
               | "+"  | "-"  | "*" | "/" ;
               
expr: var | local_var | literal | unary | binary | grouping ;
unary: ("-" | "!") expr ;
binary: expr operator expr ;
grouping: "(" expr ")" ;
var: '$' identifier ('=' stmt) ;
local_var: '#' identiifer ('=' stmt) ;

named_param: '-' identifier ;
parameter: var | local_var | string | number | cmd_call | named_param ;
cmd_call: identifier '(' (parameter | parameter+) ')' ';' ;
end_block_call: '/' identifier ;
block_call: cmd_call stmts end_block_call ;
```